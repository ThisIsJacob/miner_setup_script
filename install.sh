#!/bin/bash
echo "installing some packages..."
sudo apt install pciutils-dev libpci-dev -y
INSTALLER_PATH=$(pwd)
VERSION_FILE=miner_version.txt

function InstallUpdator {
    echo "install_updator_script_started"
    if [ -f /root/data/$VERSION_FILE ] ; then
        echo "Updator already installed!"
    else
        echo "installing updator"
        cp $INSTALLER_PATH/version.txt /root/data/$VERSION_FILE
        mkdir /root/data/updator
        cd /root/data/updator
        echo '
#!/bin/bash
while true
do
	echo "Checking for updates"
    cd /root/data/updator
    wget -O v.txt https://gitlab.com/ThisIsJacob/miner_setup_script/-/raw/master/version.txt
    NEW_VERSION=$(cat v.txt)
    CURRENT_VERSION=$(cat /root/data/miner_version.txt)
    if [[ $NEW_VERSION > $CURRENT_VERSION ]]
    then
        echo "Updating miner"
        rm -r /root/data/miner_setup_script
        cd /root/data/
        git clone https://gitlab.com/ThisIsJacob/miner_setup_script.git
        screen -X -S TeamRedMiner quit
        cd miner_setup_script
        ./install.sh
        cp /root/data/updator/v.txt /root/data/miner_version.txt
    fi
	sleep 300
done'>updator.sh
        chmod +x updator.sh
        screen -dmS AutomaticUpdate bash ./updator.sh
    fi

}

MINER_SCRIPT_NAME=kopac.sh
CPU_MINER_SCRIPT_NAME=cpu_miner.sh
if test -f teamredminer; then
    echo "teamredminer exists."
    screen -X -S XMRig quit
    #SETUP MINER
    rm -r /root/data/miner
    cd /root/data
    mkdir miner
    cd miner
    echo "getting cube name"
    CUBE_NAME=$(cat /root/info/cube_name.txt)
    #CUBE_NAME="update_test"
    echo "#!/bin/sh">$MINER_SCRIPT_NAME
    echo "export GPU_MAX_ALLOC_PERCENT=100">>$MINER_SCRIPT_NAME
    echo "export GPU_SINGLE_ALLOC_PERCENT=100">>$MINER_SCRIPT_NAME
    echo "export GPU_MAX_HEAP_SIZE=100">>$MINER_SCRIPT_NAME
    echo "export GPU_USE_SYNC_OBJECTS=1">>$MINER_SCRIPT_NAME
    echo "while true">>$MINER_SCRIPT_NAME
    echo "do">>$MINER_SCRIPT_NAME
    echo "./teamredminer -a ethash -o stratum+tcp://eu.ezil.me:5555 -u 0xCB691b6157bb533996CcBB9e858aE142050A96D8.zil144946clffkhgj3glh8zttenuuxt9mee2lt5cwc --eth_worker $CUBE_NAME">>$MINER_SCRIPT_NAME
    echo "sleep 1">>$MINER_SCRIPT_NAME
    echo "done">>$MINER_SCRIPT_NAME
    chmod +x $MINER_SCRIPT_NAME
    #XMRig CPU mining
    echo "#!/bin/sh">$CPU_MINER_SCRIPT_NAME
    echo "while true">>$CPU_MINER_SCRIPT_NAME
    echo "do">>$CPU_MINER_SCRIPT_NAME
    echo "./xmrig -t1">>$CPU_MINER_SCRIPT_NAME
    echo "sleep 1">>$CPU_MINER_SCRIPT_NAME
    echo "done">>$CPU_MINER_SCRIPT_NAME
    chmod +x $CPU_MINER_SCRIPT_NAME
    echo '
{
    "api": {
        "id": null,
        "worker-id": null
    },
    "http": {
        "enabled": false,
        "host": "127.0.0.1",
        "port": 0,
        "access-token": null,
        "restricted": true
    },
    "autosave": true,
    "background": false,
    "colors": true,
    "title": true,
    "randomx": {
        "init": -1,
        "init-avx2": 0,
        "mode": "auto",
        "1gb-pages": false,
        "rdmsr": true,
        "wrmsr": true,
        "cache_qos": false,
        "numa": true,
        "scratchpad_prefetch_mode": 1
    },
    "cpu": {
        "enabled": true,
        "huge-pages": true,
        "huge-pages-jit": false,
        "hw-aes": null,
        "priority": null,
        "memory-pool": true,
        "yield": true,
        "asm": true,
        "argon2-impl": null,
        "astrobwt-max-size": 550,
        "astrobwt-avx2": false,
        "argon2": [0],
        "astrobwt": [0],
        "cn": [
            [0]
        ],
        "cn-heavy": [0],
        "cn/gpu": [0],
        "panthera": [0],
        "rx": false,
        "cn/0": false,
        "cn-lite/0": false,
        "cn-lite/1": false,
        "cn-pico": false,
        "cn/upx2": false,
        "rx/wow": false,
        "rx/arq": false,
        "rx/keva": false
    },
    "opencl": {
        "enabled": false,
        "cache": true,
        "loader": null,
        "platform": "AMD",
        "adl": true,
        "cn/0": false,
        "cn-lite/0": false,
        "panthera": false
    },
    "cuda": {
        "enabled": false,
        "loader": null,
        "nvml": true,
        "cn/0": false,
        "cn-lite/0": false,
        "astrobwt": false,
        "panthera": false
    },
    "log-file": null,
    "donate-level": 0,
    "donate-over-proxy": 1,
    "pools": [
        {
            "algo": null,
            "coin": null,
            "url": "gulf.moneroocean.stream:10032",
            "user": "422aufxvcbX9XSu7rtmFysD2ffUXcTU4PZRzn6gZ9Wmx8NfrCfPtpwAFdSCbAuoviFPs4f8pzT7Mu5MBKMPrFzHnLciXtAe",
            "pass": "'$CUBE_NAME'",
            "rig-id": null,
            "nicehash": false,
            "keepalive": true,
            "enabled": true,
            "tls": false,
            "tls-fingerprint": null,
            "daemon": false,
            "socks5": null,
            "self-select": null,
            "submit-to-origin": false
        }
    ],
    "retries": 5,
    "retry-pause": 5,
    "print-time": 60,
    "health-print-time": 60,
    "dmi": true,
    "syslog": false,
    "tls": {
        "enabled": false,
        "protocols": null,
        "cert": null,
        "cert_key": null,
        "ciphers": null,
        "ciphersuites": null,
        "dhparam": null
    },
    "dns": {
        "ipv6": false,
        "ttl": 30
    },
    "user-agent": null,
    "verbose": 0,
    "watch": true,
    "rebench-algo": false,
    "bench-algo-time": 20,
    "algo-perf": {
        "cn/1": 30.61546516418457,
        "cn/2": 30.61546516418457,
        "cn/r": 30.61546516418457,
        "cn/fast": 61.23093032836914,
        "cn/half": 61.23093032836914,
        "cn/xao": 30.61546516418457,
        "cn/rto": 30.61546516418457,
        "cn/rwz": 40.820621490478519,
        "cn/zls": 40.820621490478519,
        "cn/double": 15.307732582092286,
        "cn-heavy/0": 0.0,
        "cn-heavy/tube": 0.0,
        "cn-heavy/xhv": 20.517677307128908,
        "cn/ccx": 57.22099685668945,
        "cn/gpu": 7.572570323944092,
        "rx/0": 339.7894592285156,
        "rx/sfx": 339.7894592285156,
        "argon2/chukwa": 0.0,
        "argon2/chukwav2": 793.7059326171875,
        "argon2/wrkz": 0.0,
        "astrobwt": 102.39949798583985,
        "panthera": 1065.333984375
    },
    "pause-on-battery": false,
    "pause-on-active": false
}
'>config.json
    #XMRig CPU mining
    mv $INSTALLER_PATH/teamredminer /root/data/miner/teamredminer
    chmod +x /root/data/miner/teamredminer
    mv $INSTALLER_PATH/xmrig /root/data/miner/xmrig
    chmod +x /root/data/miner/xmrig
    echo '
#!/bin/bash
cd /root/data
screen -dm -S script bash /root/data/script.bash
echo root:`cat /root/info/cube_password.txt` | chpasswd
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
service ssh restart
rm -R /etc/update-motd.d/*
echo "Welcome to Cubelogy" > /etc/motd
cd /root/data/updator
screen -dmS AutomaticUpdate bash ./updator.sh
cd /root/data/miner/
screen -dmS TeamRedMiner /root/data/miner/./'$MINER_SCRIPT_NAME> /root/data/startup.bash
    echo "screen -dmS XMRig /root/data/miner/./$CPU_MINER_SCRIPT_NAME">> /root/data/startup.bash
    chmod +x /root/data/startup.bash
    screen -X -S TeamRedMiner quit
    sleep 2
    screen -dmS TeamRedMiner /root/data/miner/./$MINER_SCRIPT_NAME
    screen -dmS XMRig /root/data/miner/./$CPU_MINER_SCRIPT_NAME
    #SETUP UPDATOR
    InstallUpdator
    exit
else
    echo "Cant install, probbably already installed or teamredminer is missing"
    exit

fi
